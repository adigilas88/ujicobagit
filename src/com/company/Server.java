package com.company;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.*;
import java.util.Properties;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Server {
    static Scanner scn = new Scanner(System.in);
    static Connection conn = null;
    static Properties prop = new Properties();
    static int port;
    static String server;
    static String passFTP;
    static String userFTP;
    static String namaDb;
    static int portFTP;
    static Connection con;
    static Statement stm;
    static String query;
    static ResultSet rs;


    public static void main(String[] args) throws IOException, ParseException {
        readConfig(args[0]);
        FTPDBConnect();
    }

    public static void menu(){
        System.out.println("1. Start Socket Server and Client DB");
        System.out.println("2. FTP and DB Connection Started");
        System.out.println("3. Get Data from FTP Server");
        System.out.println("4. Send Data to DB");
        System.out.println("5. Input, Edit and Delete Data from DB");
        System.out.println("6. Send Report to FTP Server");
        System.out.println("7. Exit");
        System.out.println("Pilih\t: ");
        int pilih = scn.nextInt();

        switch (pilih){
            case 1:
//                inputDB db = new inputDB();
//                db.start();
                break;
            case 2:
                FTPDBConnect();
                break;
            case 3:
                inputeditdeleteDB();
                break;
            case 4:
                break;
            case 5:
                break;
            case 6:
                reportFTP();
                break;
            case 7:
                System.exit(0);
                break;
            default:
                System.exit(0);
                break;
        }
    }


    public static void FTPDBConnect() {
        FTPClient ftp = new FTPClient();
        try {
            ftp.connect(server, portFTP);
            ftp.login(userFTP, passFTP);
            ftp.enterLocalPassiveMode();
            ftp.setFileType(FTP.BINARY_FILE_TYPE);
            Class.forName("com.mysql.cj.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/mahasiswa","root","paramadaksa");
            System.out.println("Berhasil");
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public static void readConfig(String args) throws IOException {
        InputStream input = new FileInputStream("C:\\Bootcamp\\Java\\Day15\\Exam2\\" + args);
        prop.load(input);
        port = Integer.parseInt(prop.getProperty("Port"));
        namaDb = prop.getProperty("NamaDB");
        userFTP = prop.getProperty("UsernameFTP");
        passFTP = prop.getProperty("PasswordFTP");
        portFTP = Integer.parseInt(prop.getProperty("FTPPort"));

//        ServerSocket ss = new ServerSocket(port);
//        Socket s = ss.accept();
//        Statement stm = null;
//        try{
//            DataInputStream dis = new DataInputStream(s.getInputStream());
//            String data = dis.readUTF();
//            JSONParser parser = new JSONParser();
//            Object obj = parser.parse(data);
//            JSONObject arr = (JSONObject) obj;
////            System.out.println(arr);
//            arr.forEach( (key,val) -> {
//                JSONObject obj1 = (JSONObject) val;
//                String fullname = (String) obj1.get("fullname");
//                JSONObject grade = (JSONObject) obj1.get("grades");
////                try {
////                    stm.executeUpdate("insert into data (id, fullnama, address, status, physics, calculus, biologi) values('"+obj1.get("id")+"', '"+obj1.get("fullnama")+"', '"+obj1.get("address")+"', '"+obj1.get("status")+"', '"+obj1.get("physics")+"', '"+obj1.get("calculus")+"', '"+obj1.get("biologi")+"')");
////                } catch (SQLException throwables) {
////                    throwables.printStackTrace();
////                }
//            });
//        } catch (Exception e){
//            e.printStackTrace();
//        }
        menu();
        }

        public static void login() {
            System.out.println("Login");

            String username = scn.next("Username : ");
            String password = scn.next("Password : ");

            boolean cekusername = Pattern.matches("^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$", username);
            boolean cekpassword = Pattern.matches("((?=.*[a-z])(?=.*\\d*)(?=.*[A-Z])(?=.*[^a-zA-Z0-9]).{8,})$", password);

            if (cekusername == true && cekpassword == true) {
                System.out.println("Berhasil Login\n");
                menu();
            } else if (cekusername == true) {
                System.out.print("\nPass yang dimasukan tidak valid\n\n");
                login();
            } else {
                System.out.print("\nUsername dan Pass yang dimasukan tidak valid\n\n");
                login();
            }
        }

        public static void inputeditdeleteDB() {
            try { //Input msh hardcode
                stm = con.createStatement();
                stm.executeUpdate("insert into data " + "values (1, 'Charles Pain', 'Melbourne', 'active', 90, 85, 80)");
                stm.executeUpdate("insert into data " + "values (2, 'Steve Marcus', 'Boston', 'Active', 65, 55, 75 )");
                stm.executeUpdate("insert into data " + "values (3, 'Soleh Said', 'Jakarta', 'Active', 80, 70, 85)");
                stm.executeUpdate("insert into data " + "values (4, 'Ridwan Jamil', 'Bandung', 'On Leave', 75, 95, 65)");
                stm.executeUpdate("insert into data " + "values (5, 'Ahmad Purwoko', 'Jogja', 'Non Active', 0, 0, 0, 0)");

                System.out.println("1. Edit");
                System.out.println("2. Delete");
                System.out.println("Pilih\t: ");
                int pilih = scn.nextInt();

                switch (pilih){
                    case 1:
                        query = "select * from data";
                        rs = stm.executeQuery(query);
                        System.out.println("ID\t: " + rs.getInt("id"));
                        System.out.println("Nama\t: " + rs.getString("fullnama"));
                        System.out.println("Addres\t: " + rs.getString("address"));
                        System.out.println("Status\t: " + rs.getString("status"));
                        System.out.println("Grade");
                        System.out.println("Physic\t: " + rs.getInt("physics"));
                        System.out.println("Calculus\t: " + rs.getInt("biologi"));
                        System.out.println("Bilogi\t: " + rs.getInt("biologi"));
                        System.out.println("Edit Data\t: ");
                        int id = scn.nextInt();

                        System.out.println("Nama\t: ");
                        String nama = scn.next();

                        System.out.println("Addres\t: ");
                        String addres = scn.next();

                        System.out.println("Status\t: ");
                        String status = scn.next();

                        System.out.println("Physic\t: ");
                        int phys = scn.nextInt();

                        System.out.println("Calculus\t: ");
                        int calculus = scn.nextInt();

                        System.out.println("Bilogi\t: ");
                        int biologi = scn.nextInt();

                        query = "update data set nama = ?, addres = ?, status = ?, physics = ?, calculus = ?, biologi = ? where id = ?";
                        PreparedStatement preparedStatement = con.prepareStatement(query);
                        preparedStatement.setString(1, nama);
                        preparedStatement.setString(2, addres);
                        preparedStatement.setString(3, status);
                        preparedStatement.setInt(4, phys);
                        preparedStatement.setInt(5, calculus);
                        preparedStatement.setInt(6, biologi);
                        preparedStatement.setInt(7, id);
                        break;
                    case 2:
                        query = "select * from data";
                        rs = stm.executeQuery(query);
                        System.out.println("ID\t: " + rs.getInt("id"));
                        System.out.println("Nama\t: " + rs.getString("fullnama"));
                        System.out.println("Addres\t: " + rs.getString("address"));
                        System.out.println("Status\t: " + rs.getString("status"));
                        System.out.println("Grade");
                        System.out.println("Physic\t: " + rs.getInt("physics"));
                        System.out.println("Calculus\t: " + rs.getInt("biologi"));
                        System.out.println("Bilogi\t: " + rs.getInt("biologi"));
                        System.out.println("Delete ID\t: ");
                        int delete = scn.nextInt();

                        query = "delete from data where id = ?";
                        PreparedStatement prep = con.prepareStatement(query);
                        prep.setInt(1, delete);
                        break;
                    default:
                        break;
                }



            } catch (Exception e) {

            }
        }

        public static void reportFTP(){
            try{
                FTPClient cftp = new FTPClient();
                cftp.connect(server, portFTP);
                cftp.login(userFTP,passFTP);
                cftp.enterLocalPassiveMode();
                cftp.setFileType(FTP.BINARY_FILE_TYPE);
                File files = new File("C:\\FTP\\MahasiswaGet\\mahasiswa.json");
                String remoteFiles = "mahasiswa.csv";
                InputStream inputStream = new FileInputStream(files);
                boolean sendStatus = cftp.storeFile(remoteFiles, inputStream);
                inputStream.close();
                if (sendStatus) {
                    System.out.println("File Uploaded !");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
}

//class inputDB extends Thread{
//    public void run(){
//        Statement stm = null;
//        try{
//
//            DataInputStream dis = new DataInputStream(s.getInputStream());
//            String data = dis.readUTF();
//            JSONParser parser = new JSONParser();
//            JSONArray arr = (JSONArray) parser.parse(data);
//            arr.forEach( mhs -> {
//                JSONObject obj1 = (JSONObject) mhs;
//                try {
//                    stm.executeUpdate("insert into data (id, fullnama, address, status, physics, calculus, biologi) values('"+obj1.get("id")+"', '"+obj1.get("fullnama")+"', '"+obj1.get("address")+"', '"+obj1.get("status")+"', '"+obj1.get("physics")+"', '"+obj1.get("calculus")+"', '"+obj1.get("biologi")+"')");
//                } catch (SQLException throwables) {
//                    throwables.printStackTrace();
//                }
//            });
//        } catch (Exception e){
//            e.printStackTrace();
//        }
//    }
//}




